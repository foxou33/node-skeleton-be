# Node Skeleton BE

Node.js skeleton backend application.

[![pipeline status](https://gitlab.com/foxou33/node-skeleton-be/badges/master/pipeline.svg)](https://gitlab.com/foxou33/node-skeleton-be/-/commits/master)
[![coverage report](https://gitlab.com/foxou33/node-skeleton-be/badges/master/coverage.svg)](https://gitlab.com/foxou33/node-skeleton-be/-/commits/master)

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing
purposes. See deployment for notes on how to deploy the project on a live system.

#### Prerequisites

- Node.js 12.14+
- npm 6.13+

#### Dependencies

* [Express](https://expressjs.com/) - Web framework for Node.js
* [Bootstrap](https://getbootstrap.com/) - Front-end component library

#### Installing

Install dependencies and build assets.

```bash
npm install
npm run build
```

#### Running

Start server.

```bash
npm start
```

#### Logging

Application access logs can be found in `var/log` (rotate every day).

## Development

Listen to file changes during development.

```bash
npm run build:dev
npm run start:dev
```

#### Debugging

Application use `debug` package.

```bash
DEBUG=* node ./bin/server
```

Or, use the convenience script, with hot-reloading:

```bash
npm run start:debug
```

#### Testing

Run the automated tests for this system.

```bash
npm test
```

Or, to run the tests with code coverage:

```bash
npm run coverage
```

Run the end-to-end tests:

```bash
npm run test:e2e
```

#### Documentation

More information can be found in the [DEVELOPMENT](doc/DEVELOPMENT.md) section of the documentation.

## Deployment

Set the environment variable `NODE_ENV` to production.

```bash
export NODE_ENV=production
```

Install and run the app following standard procedure.

## License

Copyright &copy; 2021 foxou33.

This project is licensed under the GNU GPL v3 License - see the [LICENSE](LICENSE) file for details.
