# Development

## Environment

Environments are defined through variables:
- development: for development purpose, logging to console
- test: testing, no console logging
- production: for deployment purpose

## Testing

Running end-to-end tests require the following browsers to be installed:
- Firefox 74+
- Chrome 80+

## Linting

Lint the code with ESLint.

```bash
npm run lint
```

## Upgrading

Upgrade dependencies using ncu.

```bash
npm run upgrade
```

## Auditing

Audit the code with npm.

```bash
npm audit
```

Run the automated fixing tool.

```bash
npm audit fix
```
