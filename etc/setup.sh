#!/usr/bin/env bash

curl -sL https://rpm.nodesource.com/setup_13.x | bash -
cat << EOF > /etc/yum.repos.d/google-chrome.repo
[google-chrome]
name=google-chrome
baseurl=http://dl.google.com/linux/chrome/rpm/stable/x86_64
enabled=1
gpgcheck=1
gpgkey=https://dl.google.com/linux/linux_signing_key.pub
EOF
dnf install -y gcc-c++ make nodejs firefox google-chrome-stable
