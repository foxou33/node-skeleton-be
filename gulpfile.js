'use strict'

// Modules.
const gulp = require('gulp')
const sass = require('gulp-sass')
const sourcemaps = require('gulp-sourcemaps')

// Locals.
sass.compiler = require('node-sass')

// Default task.
// noinspection JSUnresolvedFunction
gulp.task('default', () => gulp.src('./src/assets/scss/*.scss')
  .pipe(sourcemaps.init())
  .pipe(sass().on('error', sass.logError))
  .pipe(sourcemaps.write())
  .pipe(gulp.dest('./public/css')))

// Watch task.
gulp.task('watch', () => {
  gulp.watch('./src/assets/scss/*.scss', gulp.series('default'))
})
