// Modules.
const express = require('express')
const compression = require('compression')
const helmet = require('helmet')
const path = require('path')
const cookieParser = require('cookie-parser')
const logger = require('morgan')
const rotatingFileStream = require('rotating-file-stream')
const errorSvc = require('./services/error')
const homeRouter = require('./routes/home')
const statusRouter = require('./routes/api/status')
const configSvc = require('./services/config')

// Locals.
const app = express()

// View engine setup.
app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'pug')

// Development logger.
if (process.env.NODE_ENV === undefined || process.env.NODE_ENV === 'development') {
  app.use(logger('dev'))
}

// Access logger.
app.use(logger('combined', {
  stream: rotatingFileStream.createStream(configSvc.accessLog, {
    size: '10M',
    maxFiles: 5,
    maxSize: '50M',
    interval: '1d',
    compress: 'gzip'
  })
}))

// Middleware setup.
app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(cookieParser())
app.use(compression())
app.use(helmet())

// Static assets.
app.use(express.static(path.join(__dirname, '../public')))
app.use('/js', express.static(path.join(__dirname, '../node_modules/jquery/dist')))
app.use('/js', express.static(path.join(__dirname, '../node_modules/popper.js/dist/umd')))
app.use(express.static(path.join(__dirname, '../node_modules/bootstrap/dist')))

// Router setup.
app.use('/', homeRouter)
app.use('/api', statusRouter)

// Setup error handler.
app.use(errorSvc.notFoundError)
app.use(errorSvc.serverError)

module.exports = app
