// Modules.
const express = require('express')
const configSvc = require('../../services/config')

// Locals.
const router = express.Router()

// Status endpoint.
// noinspection JSUnresolvedFunction
router.get('/status', function (req, res) {
  res.json({
    version: configSvc.version
  })
})

module.exports = router
