// Modules.
const express = require('express')
const configSvc = require('../services/config')

// Locals.
const router = express.Router()

// Home page.
// noinspection JSUnresolvedFunction
router.get('/', function (req, res) {
  res.render('home', {
    title: 'Home',
    config: configSvc
  })
})

module.exports = router
