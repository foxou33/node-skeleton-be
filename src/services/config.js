// Modules.
const path = require('path')
const pkgInfo = require('../../package')

// Globals.
exports.appName = 'Skeleton BE'
exports.version = pkgInfo.version
exports.titleSep = ' - '
exports.copyright = 'Copyright © ' + new Date().getFullYear() + ' ' + exports.appName + '. All rights reserved.'
exports.accessLog = path.join(process.cwd(), './var/log/access.log')
exports.applicationLog = path.join(process.cwd(), './var/log/application.log')
