// Modules.
const createError = require('http-errors')
const configSvc = require('./config')

/**
 * Not found error.
 *
 * @param {object} req
 * @param {object} res
 */
exports.notFoundError = (req, res) => {
  const err = createError(404)

  // Render page not found.
  res.status(err.status || 404)
  res.render('error', {
    config: configSvc,
    title: err.message.toLowerCase().charAt(0).toUpperCase() + err.message.toLowerCase().slice(1),
    message: err.message,
    error: err
  })
}

/**
 * Server error.
 *
 * @param {object} err
 * @param {object} req
 * @param {object} res
 */
exports.serverError = (err, req, res) => {
  res.locals.message = err.message
  res.locals.error = req.app.get('env') === 'development' ? err : {}

  // Render error page.
  res.status(err.status || 500)
  res.render('error', {
    config: configSvc,
    title: err.message
  })
}
