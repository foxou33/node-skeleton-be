// Modules.
const configSvc = require('./config')
const debug = require('debug')(`${configSvc.appName}:util`)
const winston = require('winston')
const http = require('http')
const app = require('../app')

// Globals.
// noinspection JSCheckFunctionSignatures
exports.logger = winston.createLogger({
  level: 'silly',
  format: winston.format.combine(
    winston.format.timestamp(),
    winston.format.json()
  ),
  transports: [
    new winston.transports.Console({
      format: winston.format.combine(
        winston.format.colorize(),
        winston.format.printf(info => `${info.timestamp}: ${info.level}: ${info.message}`)
      )
    }),
    new winston.transports.File({
      filename: configSvc.applicationLog
    })
  ]
})
exports.server = null
exports.port = null

/**
 * Init.
 */
exports.init = () => {
  // Create HTTP server.
  exports.server = http.createServer(app)

  // Init app.
  app.set('port', exports.normalizePort(process.env.PORT || '3000'))
  exports.server.on('error', exports.onError)
  exports.server.on('listening', exports.onListening)
  exports.server.listen(app.get('port'))
}

/**
 * Exit error.
 *
 * @param {string} message
 * @param {number} code
 */
exports.exitError = (message, code) => {
  exports.logger.error(message)
  exports.logger.on('finish', () => {
    process.exit(code)
  })
  exports.logger.end()
}

/**
 * Normalize port.
 *
 * @param {string} val
 * @returns {boolean|number|*}
 */
exports.normalizePort = (val) => {
  const port = parseInt(val, 10)

  if (isNaN(port)) {
    exports.port = val

    return val
  }

  if (port >= 0) {
    exports.port = port

    return port
  }

  return false
}

/**
 * On error.
 *
 * @param {object} error
 * @throws Error
 */
exports.onError = (error) => {
  if (error.syscall !== 'listen') {
    throw error
  }

  const bind = typeof exports.port === 'string'
    ? 'Pipe ' + exports.port
    : 'Port ' + exports.port

  // Handle specific listen errors.
  switch (error.code) {
    case 'EACCES':
      exports.exitError(bind + ' requires elevated privileges', 1)
      break
    case 'EADDRINUSE':
      exports.exitError(bind + ' is already in use', 1)
      break
    default:
      throw error
  }
}

/**
 * On listening.
 */
exports.onListening = () => {
  const addr = exports.server.address()
  const bind = typeof addr === 'string'
    ? 'pipe ' + addr
    : 'port ' + addr.port

  debug(`Listening on ${bind}`)
}
