// Modules.
const mocha = require('mocha')
const chai = require('chai')
const chaiHttp = require('chai-http')
const app = require('../../src/app')
const configSvc = require('../../src/services/config')

// Locals.
const describe = mocha.describe
const it = mocha.it
const expect = chai.expect

chai.use(chaiHttp)

describe('Test status endpoint', function () {
  it('Should respond HTTP 200 OK', function (done) {
    chai.request(app).get('/api/status').end(function (err, res) {
      if (err) {
        return done(err)
      }

      // noinspection JSUnresolvedVariable
      expect(res).to.have.status(200)
      // noinspection JSUnresolvedVariable
      expect(res.body).to.have.property('version')
      expect(res.body.version).to.equal(configSvc.version)
      done()
    })
  })
})
