// Modules.
const configSvc = require('../../src/services/config')
const utilSvc = require('../../src/services/util')

// Globals.
// noinspection JSUnusedGlobalSymbols
module.exports = {
  before: function (browser, done) {
    utilSvc.init()
    done()
  },
  after: function () {
    utilSvc.server.close()
  },
  'Test home page': function (browser) {
    // noinspection JSUnresolvedFunction
    browser
      .url('http://localhost:3000')
      .waitForElementVisible('body')
      .assert.titleContains(configSvc.appName + configSvc.titleSep + 'Home')
      .assert.containsText('header.masthead h3.masthead-brand', configSvc.appName)
      .assert.visible('body main.inner.cover button img')
      .assert.containsText('footer.mastfoot p', configSvc.copyright)
      .click('body main.inner.cover button')
      .waitForElementVisible('#config')
      .assert.containsText('#config .card-body .card-text', configSvc.version)
      .end()
  }
}
