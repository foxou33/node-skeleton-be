// Modules.
const configSvc = require('../../src/services/config')
const utilSvc = require('../../src/services/util')

// Globals.
module.exports = {
  before: function (browser, done) {
    utilSvc.init()
    done()
  },
  after: function () {
    utilSvc.server.close()
  },
  'Test not found page': function (browser) {
    // noinspection JSUnresolvedFunction
    browser
      .url('http://localhost:3000/not-found')
      .waitForElementVisible('body')
      .assert.titleContains(configSvc.appName + configSvc.titleSep + 'Not found')
      .assert.containsText('header a.navbar-brand', configSvc.appName)
      .assert.containsText('footer span', configSvc.copyright)
      .end()
  }
}
