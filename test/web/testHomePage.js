// Modules.
const mocha = require('mocha')
const chai = require('chai')
const chaiHttp = require('chai-http')
const app = require('../../src/app')

// Locals.
const describe = mocha.describe
const it = mocha.it
const expect = chai.expect

chai.use(chaiHttp)

describe('Test home page', function () {
  it('Should respond HTTP 200 OK', function (done) {
    chai.request(app).get('/').end(function (err, res) {
      if (err) {
        return done(err)
      }

      // noinspection JSUnresolvedVariable
      expect(res).to.have.status(200)
      done()
    })
  })
})
