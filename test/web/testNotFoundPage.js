// Modules.
const mocha = require('mocha')
const chai = require('chai')
const chaiHttp = require('chai-http')
const app = require('../../src/app')

// Locals.
const describe = mocha.describe
const it = mocha.it
const expect = chai.expect

chai.use(chaiHttp)

describe('Test not found page', function () {
  it('Should respond HTTP 404 NOT FOUND', function (done) {
    chai.request(app).get('/not-found').end(function (err, res) {
      if (err) {
        return done(err)
      }

      // noinspection JSUnresolvedVariable
      expect(res).to.have.status(404)
      done()
    })
  })
})
